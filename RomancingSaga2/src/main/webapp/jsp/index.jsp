<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>ProjectAvalon</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>

    <body background="${pageContext.request.contextPath}/img/rs2splash.jpg">
        <nav role="navigation" class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand navbar-brand-centered" href="${pageContext.request.contextPath}/">SaGaDB</a>
                </div>
                <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li id="homeNav"><a href="${pageContext.request.contextPath}/">Home</a></li>
                        <li id="heroesNav"><a href="${pageContext.request.contextPath}/characters">Characters</a></li>
                        <li id="powersNav"><a href="${pageContext.request.contextPath}/techs">Techs</a></li>
                        <li id="sightingsNav"><a href="${pageContext.request.contextPath}/monsters">Monsters</a></li>
                        <li id="locationsNav"><a href="${pageContext.request.contextPath}/sparkassist">SparkAssist</a></li>
                        <li id="orgsNav"><a href="${pageContext.request.contextPath}/about">About</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Edit<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Edit Heroes</a></li>
                                <li><a href="#">Edit Powers</a></li>
                                <li><a href="#">Edit Sightings</a></li>
                                <li><a href="#">Edit Locations</a></li>
                                <li><a href="#">Edit Organizations</a></li>
                            </ul>
                        </li>

                    </ul>


                </div>
            </div>
        </nav>




        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>