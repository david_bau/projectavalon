<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<html>


    <head>
        <title>ProjectAvalon</title> 
        <!-- JQuery -->
        <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/styleSheett.css">


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.0/js/jquery.tablesorter.js" integrity="sha256-serXvhbeEKdQIfTFSD3wpNCGNx2+/9py7VXfwLhYTfk="
        crossorigin="anonymous"></script>

        <!--        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.0/css/theme.bootstrap_3.min.css"
                      integrity="sha256-dXZ9g5NdsPlD0182JqLz9UFael+Ug5AYo63RfujWPu8=" crossorigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.0/css/theme.default.min.css"
                      integrity="sha256-kZJ4kB78IbXuxMtCpmaXzii8vxEKtu8pjicH62E0/qM=" crossorigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.0/css/theme.bootstrap.min.css"
                      integrity="sha256-dXZ9g5NdsPlD0182JqLz9UFael+Ug5AYo63RfujWPu8=" crossorigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.0/css/jquery.tablesorter.pager.min.css"
                      integrity="sha256-x+whz5gQKEXx3S3pxwmxPhC1OWpRiHaPXUW5Yt8/fzg=" crossorigin="anonymous" />-->
        <style>
        </style>
        <script>

            $(document).ready(function () {
                $("#myTable").tablesorter();
            });
        </script>
        <script>
            $('#example tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
        </script>
    </head>

    <body background="${pageContext.request.contextPath}/img/rs2bg.jpg">

        <nav role="navigation" class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand navbar-brand-centered" href="${pageContext.request.contextPath}/">SaGaDB</a>
                </div>
                <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li id="homeNav"><a href="${pageContext.request.contextPath}/">Home</a></li>
                        <li id="heroesNav"><a href="${pageContext.request.contextPath}/characters">Characters</a></li>
                        <li id="powersNav"><a href="${pageContext.request.contextPath}/techs">Techs</a></li>
                        <li id="sightingsNav"><a href="${pageContext.request.contextPath}/monsters">Monsters</a></li>
                        <li id="locationsNav"><a href="${pageContext.request.contextPath}/sparkassist">SparkAssist</a></li>
                        <li id="orgsNav"><a href="${pageContext.request.contextPath}/about">About</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Edit<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Edit Heroes</a></li>
                                <li><a href="#">Edit Powers</a></li>
                                <li><a href="#">Edit Sightings</a></li>
                                <li><a href="#">Edit Locations</a></li>
                                <li><a href="#">Edit Organizations</a></li>
                            </ul>
                        </li>

                    </ul>


                </div>
            </div>
        </nav>


        <div class="rounded">
            <h2 align="center">${charclass}</h2>
            <p align="center">Click the name of any column to re-sort the table.
                <br/>
                DOMINANT TYPE | ${typeString} <br/>
                =Averages=<br/>
                LP: ${averages[1]} | 
                Strength: ${averages[2]} | 
                Dexterity: ${averages[3]} |
                Magic: ${averages[4]} |
                Logic: ${averages[5]} | 
                Speed: ${averages[6]} | 
                Constitution: ${averages[7]}



            </p>
        </div>


        <div>
            <div class="col-md-5 container-fluid">
                <table align="left" class="table" id="oneRowTable">   
                    <thead>

                        <tr>
                            <th width="1%">ID</th>
                            <th width="7%">Name</th>
                            <th width="3%">Class</th>
                            <!-- 35 --> 
                            <th width="3%">Female?</th>
                            <th width="3%">Undead?</th>
                            <th width="3%">Ghost?</th>
                            <th width="3%">Flight?</th>
                            <!-- 55 -->
                            <th width=5%">Race</th>
                            <th width=1%">SpkType</th>

                        </tr>
                    </thead>

                    <tr>

                        <!-- Modal -->

                        <td>
                            <c:out value="${currentChar.pcid}"/>
                        </td>
                        <td>


                            <a href="getCharacter?charname=${currentChar.charname}">


                                <c:out value="${currentChar.charname}"/></a>
                        </td>
                        <td>


                            <a href="displayClass?charclass=${currentChar.charclass}">
                                <c:out value="${currentChar.charclass}"/></a>
                        </td>

                        <td>
                            <c:out value="${currentChar.female}"/>
                        </td>
                        <td>
                            <c:out value="${currentChar.undead}"/>
                        </td>
                        <td>
                            <c:out value="${currentChar.ghost}"/>
                        </td>                        
                        <td>
                            <c:out value="${currentChar.flying}"/>
                        </td>
                        <td>
                            <c:out value="${currentChar.race}"/>
                        </td>
                        <td>
                            <c:out value="${currentChar.sparktype}"/>
                        </td>


                    </tr>


                </table>
            </div>
            <div>
                <c:if test = "${ not empty charname}">
                    <p>
                        <br/>
                        <br/>
                        <br/> <br/>
                        <br/>
                    </p>
                    <p class="chardeets col-md-9 container-fluid"> 
                        =Rankings=<br/>
                        LP - ${rankings[0]} of 278 | 
                        Str - ${rankings[1]} of 278 | Dex - [WIP]</br>



                    </p>
                </c:if>

            </div>
        </div>

        <div class="col-md-9 container-fluid">
            <table align="center" class="table table-hover tablesorter" id="myTable">   
                <thead>
                    <tr>
                        <th width="1%">ID</th>
                        <th width="7%">Name</th>
                        <th width="3%">Class</th>
                        <!-- 35 --> 
                        <!--                    <th width="3%">Female?</th>
                                            <th width="3%">Undead?</th>
                                            <th width="3%">Ghost?</th>
                                            <th width="3%">Flight?</th>-->
                        <!-- 55 -->
                        <th width=5%">Race</th>
                        <th width=1%">SpkType</th>
                        <th width="1%">LP</th>
                        <th width="1%">Str</th>
                        <th width="1%">Dex</th>
                        <th width="1%">Mag</th>
                        <th width="1%">Lgc</th>
                        <th width="1%">Spd</th>
                        <th width="1%">Con</th>
                        <th width="1%">SpP</th>
                        <th width="1%">Swd</th>
                        <th width="1%">Spr</th>
                        <th width="1%">Axe</th>
                        <th width="1%">Bow</th>
                        <th width="1%">Fst</th>
                        <th width="1%">Pyr</th>
                        <th width="1%">Hyd</th>
                        <th width="1%">Aer</th>
                        <th width="1%">Ter</th>
                        <th width="1%">Lgh</th>
                        <th width="1%">Drk</th>

                    </tr>
                </thead>
                <c:forEach var="thisCharacter" items="${classCharList}">
                    <tr>
                        <td>
                            <c:out value="${thisCharacter.pcid}"/>
                        </td>
                        <td>


                            <a href="getClassCharacter?charname=${thisCharacter.charname}">


                                <c:out value="${thisCharacter.charname}"/></a>
                        </td>
                        <td>


                            <a href="displayClass?charclass=${thisCharacter.charclass}">
                                <c:out value="${thisCharacter.charclass}"/></a>
                        </td>

                        <!--                        <td>
                        <c:out value="${thisCharacter.female}"/>
                    </td>
                    <td>
                        <c:out value="${thisCharacter.undead}"/>
                    </td>
                    <td>
                        <c:out value="${thisCharacter.ghost}"/>
                    </td>                        
                    <td>
                        <c:out value="${thisCharacter.flying}"/>
                    </td>-->
                        <td>
                            <c:out value="${thisCharacter.race}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.sparktype}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.lp}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.str}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.dex}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.mag}"/>
                        </td>                        
                        <td>
                            <c:out value="${thisCharacter.logic}"/>
                        </td>

                        <td>
                            <c:out value="${thisCharacter.spd}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.con}"/>
                        </td>                        
                        <td>
                            <c:out value="${thisCharacter.spellpower}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.sword}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.spear}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.axe}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.bow}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.unarmed}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.fire}"/>
                        </td>                        
                        <td>
                            <c:out value="${thisCharacter.water}"/>
                        </td>

                        <td>
                            <c:out value="${thisCharacter.wind}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.earth}"/>
                        </td>
                        <td>
                            <c:out value="${thisCharacter.light}"/>
                        </td>                        
                        <td>
                            <c:out value="${thisCharacter.dark}"/>
                        </td>

                    </tr>
                </c:forEach>
            </table>
        </div>
        <br/>
        <br/>


        <!-- Placed at the end of the document so the pages load faster -->
    </body>





</html>