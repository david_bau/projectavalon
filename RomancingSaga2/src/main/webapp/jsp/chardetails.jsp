<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<html>


    <head>
        <title>ProjectAvalon</title> 
        <!-- JQuery -->
        <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/styleSheett.css">


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.0/js/jquery.tablesorter.js" integrity="sha256-serXvhbeEKdQIfTFSD3wpNCGNx2+/9py7VXfwLhYTfk="
        crossorigin="anonymous"></script>

        <!--        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.0/css/theme.bootstrap_3.min.css"
                      integrity="sha256-dXZ9g5NdsPlD0182JqLz9UFael+Ug5AYo63RfujWPu8=" crossorigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.0/css/theme.default.min.css"
                      integrity="sha256-kZJ4kB78IbXuxMtCpmaXzii8vxEKtu8pjicH62E0/qM=" crossorigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.0/css/theme.bootstrap.min.css"
                      integrity="sha256-dXZ9g5NdsPlD0182JqLz9UFael+Ug5AYo63RfujWPu8=" crossorigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.0/css/jquery.tablesorter.pager.min.css"
                      integrity="sha256-x+whz5gQKEXx3S3pxwmxPhC1OWpRiHaPXUW5Yt8/fzg=" crossorigin="anonymous" />-->
        <style>
        </style>
        <script>

            $(document).ready(function () {
                $("#myTable").tablesorter();
            });
        </script>
    </head>

    <body background="${pageContext.request.contextPath}/img/rs2bg.jpg">

        <nav role="navigation" class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand navbar-brand-centered" href="${pageContext.request.contextPath}/">SaGaDB</a>
                </div>
                <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li id="homeNav"><a href="${pageContext.request.contextPath}/">Home</a></li>
                        <li id="heroesNav"><a href="${pageContext.request.contextPath}/characters">Characters</a></li>
                        <li id="powersNav"><a href="${pageContext.request.contextPath}/techs">Techs</a></li>
                        <li id="sightingsNav"><a href="${pageContext.request.contextPath}/monsters">Monsters</a></li>
                        <li id="locationsNav"><a href="${pageContext.request.contextPath}/sparkassist">SparkAssist</a></li>
                        <li id="orgsNav"><a href="${pageContext.request.contextPath}/about">About</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Edit<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Edit Heroes</a></li>
                                <li><a href="#">Edit Powers</a></li>
                                <li><a href="#">Edit Sightings</a></li>
                                <li><a href="#">Edit Locations</a></li>
                                <li><a href="#">Edit Organizations</a></li>
                            </ul>
                        </li>

                    </ul>


                </div>
            </div>
        </nav>



    <div ng-mouseover="chardetails" class="col-md-9 container-fluid">
               
                    <h2 align="left">${charname}</h2>
                    <p align="">
                        <br/>
                       =Rankings=<br/>
                       LP - ${rankings[0]} of 278 </br>
                      Str - ${rankings[1]} of 278</br>
                       
                             
                       
                    </p>

        </div>
        <br/>
        <br/>


        <!-- Placed at the end of the document so the pages load faster -->
    </body>





</html>