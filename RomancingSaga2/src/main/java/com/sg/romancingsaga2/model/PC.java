/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.romancingsaga2.model;

import java.util.Objects;

/**
 *
 * @author dbb09
 */
public class PC {

    private int pcid;
    private String  charname;
    private String charclass;
    private boolean female;
    private boolean undead;
    private boolean ghost;
    private boolean flying;
    private String race;
    private int sparktype;
    private int lp;
    private int str;
    private int dex;
    private int mag;
    private int logic;
    private int spd;
    private int con;
    private int spellpower;
    private int sword;
    private int spear;
    private int axe;
    private int bow;
    private int unarmed;
    private int fire;
    private int water;
    private int wind;
    private int earth;
    private int light;
    private int dark;

    public int getPcid() {
        return pcid;
    }

    public void setPcid(int pcid) {
        this.pcid = pcid;
    }

    public String getCharname() {
        return charname;
    }

    public void setCharname(String charname) {
        this.charname = charname;
    }

    public String getCharclass() {
        return charclass;
    }

    public void setCharclass(String charclass) {
        this.charclass = charclass;
    }

    public boolean isFemale() {
        return female;
    }

    public void setFemale(boolean female) {
        this.female = female;
    }

    public boolean isUndead() {
        return undead;
    }

    public void setUndead(boolean undead) {
        this.undead = undead;
    }

    public boolean isGhost() {
        return ghost;
    }

    public void setGhost(boolean ghost) {
        this.ghost = ghost;
    }

    public boolean isFlying() {
        return flying;
    }

    public void setFlying(boolean flying) {
        this.flying = flying;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public int getSparktype() {
        return sparktype;
    }

    public void setSparktype(int sparktype) {
        this.sparktype = sparktype;
    }

    public int getLp() {
        return lp;
    }

    public void setLp(int lp) {
        this.lp = lp;
    }

    public int getStr() {
        return str;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public int getDex() {
        return dex;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public int getMag() {
        return mag;
    }

    public void setMag(int mag) {
        this.mag = mag;
    }

    public int getLogic() {
        return logic;
    }

    public void setLogic(int logic) {
        this.logic = logic;
    }

    public int getSpd() {
        return spd;
    }

    public void setSpd(int spd) {
        this.spd = spd;
    }

    public int getCon() {
        return con;
    }

    public void setCon(int con) {
        this.con = con;
    }

    public int getSpellpower() {
        return spellpower;
    }

    public void setSpellpower(int spellpower) {
        this.spellpower = spellpower;
    }

    public int getSword() {
        return sword;
    }

    public void setSword(int sword) {
        this.sword = sword;
    }

    public int getSpear() {
        return spear;
    }

    public void setSpear(int spear) {
        this.spear = spear;
    }

    public int getAxe() {
        return axe;
    }

    public void setAxe(int axe) {
        this.axe = axe;
    }

    public int getBow() {
        return bow;
    }

    public void setBow(int bow) {
        this.bow = bow;
    }

    public int getUnarmed() {
        return unarmed;
    }

    public void setUnarmed(int unarmed) {
        this.unarmed = unarmed;
    }

    public int getFire() {
        return fire;
    }

    public void setFire(int fire) {
        this.fire = fire;
    }

    public int getWater() {
        return water;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public int getWind() {
        return wind;
    }

    public void setWind(int wind) {
        this.wind = wind;
    }

    public int getEarth() {
        return earth;
    }

    public void setEarth(int earth) {
        this.earth = earth;
    }

    public int getLight() {
        return light;
    }

    public void setLight(int light) {
        this.light = light;
    }

    public int getDark() {
        return dark;
    }

    public void setDark(int dark) {
        this.dark = dark;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.pcid;
        hash = 59 * hash + Objects.hashCode(this.charname);
        hash = 59 * hash + Objects.hashCode(this.charclass);
        hash = 59 * hash + (this.female ? 1 : 0);
        hash = 59 * hash + (this.undead ? 1 : 0);
        hash = 59 * hash + (this.ghost ? 1 : 0);
        hash = 59 * hash + (this.flying ? 1 : 0);
        hash = 59 * hash + Objects.hashCode(this.race);
        hash = 59 * hash + this.sparktype;
        hash = 59 * hash + this.lp;
        hash = 59 * hash + this.str;
        hash = 59 * hash + this.dex;
        hash = 59 * hash + this.mag;
        hash = 59 * hash + this.logic;
        hash = 59 * hash + this.spd;
        hash = 59 * hash + this.con;
        hash = 59 * hash + this.spellpower;
        hash = 59 * hash + this.sword;
        hash = 59 * hash + this.spear;
        hash = 59 * hash + this.axe;
        hash = 59 * hash + this.bow;
        hash = 59 * hash + this.unarmed;
        hash = 59 * hash + this.fire;
        hash = 59 * hash + this.water;
        hash = 59 * hash + this.wind;
        hash = 59 * hash + this.earth;
        hash = 59 * hash + this.light;
        hash = 59 * hash + this.dark;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PC other = (PC) obj;
        if (this.pcid != other.pcid) {
            return false;
        }
        if (this.female != other.female) {
            return false;
        }
        if (this.undead != other.undead) {
            return false;
        }
        if (this.ghost != other.ghost) {
            return false;
        }
        if (this.flying != other.flying) {
            return false;
        }
        if (this.sparktype != other.sparktype) {
            return false;
        }
        if (this.lp != other.lp) {
            return false;
        }
        if (this.str != other.str) {
            return false;
        }
        if (this.dex != other.dex) {
            return false;
        }
        if (this.mag != other.mag) {
            return false;
        }
        if (this.logic != other.logic) {
            return false;
        }
        if (this.spd != other.spd) {
            return false;
        }
        if (this.con != other.con) {
            return false;
        }
        if (this.spellpower != other.spellpower) {
            return false;
        }
        if (this.sword != other.sword) {
            return false;
        }
        if (this.spear != other.spear) {
            return false;
        }
        if (this.axe != other.axe) {
            return false;
        }
        if (this.bow != other.bow) {
            return false;
        }
        if (this.unarmed != other.unarmed) {
            return false;
        }
        if (this.fire != other.fire) {
            return false;
        }
        if (this.water != other.water) {
            return false;
        }
        if (this.wind != other.wind) {
            return false;
        }
        if (this.earth != other.earth) {
            return false;
        }
        if (this.light != other.light) {
            return false;
        }
        if (this.dark != other.dark) {
            return false;
        }
        if (!Objects.equals(this.charname, other.charname)) {
            return false;
        }
        if (!Objects.equals(this.charclass, other.charclass)) {
            return false;
        }
        if (!Objects.equals(this.race, other.race)) {
            return false;
        }
        return true;
    }
    
    
    
    
    
}
