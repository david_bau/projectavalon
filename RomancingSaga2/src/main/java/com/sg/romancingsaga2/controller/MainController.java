/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.romancingsaga2.controller;

import com.sg.romancingsaga2.dao.PCDao;
import com.sg.romancingsaga2.model.PC;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    private final PCDao pcdao;
//    private final techDao tdao;
//    private final MonDao mdao;
//    private final ServiceLayer svc;

    @Inject
    public MainController(PCDao pcdao
    //           , techDao tdao,
    //            monDao mdao

    ) {
        this.pcdao = pcdao;
//        this.tdao = tdao;
//        this.mdao = mdao;
    }

    int cB1 = 0;
    int cB2 = 0;
    int cB3 = 0;
    int cB4 = 0;
    int cB5 = 0;

    int cB9 = 0;

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayIndexPage(Model model) {

        return "index";

    }

    @RequestMapping(value = {"/characters"}, method = RequestMethod.GET)
    public String displayCharacterPage(Model model) {

        List<PC> characters = pcdao.getAllPCs();
        model.addAttribute("characters", characters);

        return "characters";

    }

    @RequestMapping(value = "displayClass", method = RequestMethod.GET)
    public String displayClass(HttpServletRequest request, Model model) {

        String charclass = request.getParameter("charclass");
        List<Integer> averages = loadClassSnippet(charclass);
        String typeString = typeConverter(averages.get(0));

//        Post post = pdao.getPostById(postID);
        List<PC> classCharList = pcdao.getPCsOfClass(charclass);

        model.addAttribute("classCharList", classCharList);
        model.addAttribute("charclass", charclass);
        model.addAttribute("averages", averages);
        model.addAttribute("typeString", typeString);
//        model.addAttribute("post", post);

        return "classes";
    }

    @RequestMapping(value = "getCharacter", method = RequestMethod.GET)
    public String displayCharacter(HttpServletRequest request, Model model) {

        String charname = request.getParameter("charname");
        PC currentChar = pcdao.getPCByName(charname);
        List<Integer> rankings = loadRankings(charname);
//        String typeString = typeConverter(averages.get(0));

//        Post post = pdao.getPostById(postID);
        model.addAttribute("charname", charname);
        model.addAttribute("rankings", rankings);
        List<PC> characters = pcdao.getAllPCs();
        model.addAttribute("characters", characters);
        model.addAttribute("currentChar", currentChar);
//        model.addAttribute("post", post);

        return "characters";
    }

    @RequestMapping(value = "getClassCharacter", method = RequestMethod.GET)
    public String displayClassCharacter(HttpServletRequest request, Model model) {

        String charname = request.getParameter("charname");
        PC currentChar = pcdao.getPCByName(charname);
        String charclass = currentChar.getCharclass();
        List<Integer> rankings = loadRankings(charname);
        List<PC> classCharList = pcdao.getPCsOfClass(charclass);
//        String typeString = typeConverter(averages.get(0));

//        Post post = pdao.getPostById(postID);
        model.addAttribute("charname", charname);
        model.addAttribute("charclass", charclass);
        model.addAttribute("rankings", rankings);

        model.addAttribute("classCharList", classCharList);
        model.addAttribute("currentChar", currentChar);
//        model.addAttribute("post", post);

        return "classes";
    }

    public List<Integer> loadRankings(String charname) {
        PC thisChar = pcdao.getPCByName(charname);
        List<PC> allChars = pcdao.getAllPCs();
        List<Integer> rankingList = new ArrayList<>();
        int LPRank = 1;
        int charLp = thisChar.getLp();
        for (PC pc : allChars) {
            if (charLp < pc.getLp()) {
                LPRank++;
            }
        }
        rankingList.add(LPRank);

        int StrRank = 1;
        int charStr = thisChar.getStr();
        for (PC pc : allChars) {
            if (charStr < pc.getStr()) {
                StrRank++;
            }
        }
        rankingList.add(StrRank);

        return rankingList;
    }

    public String typeConverter(Integer type) {
        String typeReturn = "";
        if (type == 9) {
            typeReturn = "09 | Polearm (Axe/Spear)";
        }
        return typeReturn;
    }

    public List<Integer> loadClassSnippet(String charclass) {
        List<PC> charList = pcdao.getPCsOfClass(charclass);
        List<Integer> avgList = new ArrayList<>();

        //AvgAxe
        int counter = 0;
        int var = 0;
        List<Integer> numbers = new ArrayList<>();

        for (PC pc : charList) {
            numbers.add(pc.getSparktype());
        }

        List<Integer> sparkmodal = modal(numbers);
        int dominantST = sparkmodal.get(0);
        avgList.add(dominantST);

        //AvgLP
        counter = 0;
        var = 0;
        for (PC pc : charList) {
            var = var + pc.getLp();
            counter++;
        }
        int avgLP = var / counter;
        avgList.add(avgLP);

        //AvgStr
        counter = 0;
        var = 0;
        for (PC pc : charList) {
            var = var + pc.getStr();
            counter++;
        }
        int avgStr = var / counter;
        avgList.add(avgStr);

        //AvgDex
        counter = 0;
        var = 0;
        for (PC pc : charList) {
            var = var + pc.getDex();
            counter++;
        }
        int avgDex = var / counter;
        avgList.add(avgDex);

        //AvgMag
        counter = 0;
        var = 0;
        for (PC pc : charList) {
            var = var + pc.getMag();
            counter++;
        }
        int avg = var / counter;
        avgList.add(avg);

        //AvgLgc
        counter = 0;
        var = 0;
        avg = 0;
        for (PC pc : charList) {
            var = var + pc.getLogic();
            counter++;
        }
        avg = var / counter;
        avgList.add(avg);

        //AvgSpd
        counter = 0;
        var = 0;
        for (PC pc : charList) {
            var = var + pc.getSpd();
            counter++;
        }
        avg = var / counter;
        avgList.add(avg);
        //AvgCon
        counter = 0;
        var = 0;
        for (PC pc : charList) {
            var = var + pc.getCon();
            counter++;
        }
        avg = var / counter;
        avgList.add(avg);

        //return
        return avgList;
    }

    public static List<Integer> modal(final List<Integer> numbers) {
        final List<Integer> modes = new ArrayList<Integer>();
        final Map<Integer, Integer> countMap = new HashMap<Integer, Integer>();

        int max = -1;

        for (final int n : numbers) {
            int count = 0;

            if (countMap.containsKey(n)) {
                count = countMap.get(n) + 1;
            } else {
                count = 1;
            }

            countMap.put(n, count);

            if (count > max) {
                max = count;
            }
        }

        for (final Map.Entry<Integer, Integer> tuple : countMap.entrySet()) {
            if (tuple.getValue() == max) {
                modes.add(tuple.getKey());
            }
        }

        return modes;
    }

//    @RequestMapping(value = "displayPost", method = RequestMethod.GET)
//    public String displayPost(HttpServletRequest request, Model model) {
//
//        int postID = Integer.parseInt(request.getParameter("postID"));
//
//        Post post = pdao.getPostById(postID);
//
//        List<Image> postImageList = idao.getPostImagesByPostID(postID);
//
//        model.addAttribute("postImageList", postImageList);
//        model.addAttribute("post", post);
//
//        return "blogPost";
//    }
//    @RequestMapping(value = "/classes", method = GET)
//    @ResponseBody
//    public String getClassPage(
//            @RequestParam("charclass") String charclass, Model model) {
//
//        List<PC> charactersOfClass = pcdao.getPCsOfClass(charclass);
//        model.addAttribute("charactersOfClass", charactersOfClass);
//        return "Get a specific Class with name=" + charclass;
//    }
//    @RequestMapping(value = "/ex/foos/{id}", method = GET)
//    @ResponseBody
//    public String getFoosBySimplePathWithPathVariable(
//            @PathVariable String id) {
//        return "Get a specific Foo with id=" + id;
//    
//    @RequestMapping(value = "/**", method = RequestMethod.GET)
//    public String display404Page(Model model) {
//
//        String pageNotFound = "404 Page Not Found";
//
//        model.addAttribute("pageNotFound", pageNotFound);
//
//        return "uhoh";
//    }
//
//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public String displayLoginPage() {
//
//        return "login";
//    }
//
//    @RequestMapping(value = "/about", method = RequestMethod.GET)
//    public String displayAboutPage(Model model) {
//        return "about";
//    }
//
//    @RequestMapping(value = "/falcons", method = RequestMethod.GET)
//    public String displayFalconsPage(Model model) {
//        List<Bird> birdList = bdao.getAllBirds();
//        List<Image> imageList = idao.getAllImages();
//
//        model.addAttribute("birdList", birdList);
//        model.addAttribute("imageList", imageList);
//
//        return "falcons";
//    }
//
//    @RequestMapping(value = "/createFalcon", method = RequestMethod.POST)
//    public String createFalcon(HttpServletRequest request, Model model) {
//        try {
//
//            boolean existingLocation = false;
//
//            Bird birdToAdd = new Bird();
//            birdToAdd.setBirdName(request.getParameter("birdName"));
//            birdToAdd.setBirdDesc(request.getParameter("birdDesc"));
//            birdToAdd.setSpecies(request.getParameter("birdSpecies"));
//            birdToAdd.setHandler(request.getParameter("birdHandler"));
//            String locationName = request.getParameter("location");
//
//            List<Location> knownLocations = ldao.getAllLocations();
//
//            for (Location currentLocation : knownLocations) {
//                if (currentLocation.getLocationName().equals(locationName)) {
//                    birdToAdd.setLocation(ldao.getLocation(currentLocation.getLocationID()));
//                    existingLocation = true;
//                }
//            }
//
//            if (!existingLocation) {
//                birdToAdd.setLocation(svc.formatLocation(locationName));
//            }
//
//            birdToAdd.setImgID(1);
//
//            bdao.addBird(birdToAdd);
//            return "redirect:falcons";
//
//        } catch (InvalidFormatException | NullPointerException | IndexOutOfBoundsException e) {
//            model.addAttribute("exception", e);
//            return "uhoh";
//        } catch (DataIntegrityViolationException e) {
//            String errormsg = "You entered too much data into one or more field.  Please try again.";
//            model.addAttribute("exception", errormsg);
//            return "uhoh";
//        }
//    }
//
//    @RequestMapping(value = "/updateFalcon", method = RequestMethod.POST)
//    public String updateFalcon(HttpServletRequest request) {
//
//        boolean existingLocation = false;
//
//        int birdID = Integer.parseInt(request.getParameter("birdID"));
//        String name = request.getParameter("birdName");
//        String desc = request.getParameter("birdDesc");
//        String species = request.getParameter("birdSpecies");
//        String handler = request.getParameter("birdHandler");
//        String location = request.getParameter("birdLocation");
//        String newContent = request.getParameter("birdImg");
//
//        Bird oldBird = bdao.getBirdByID(birdID);
//
//        List<Location> knownLocations = ldao.getAllLocations();
//
//        for (Location currentLocation : knownLocations) {
//            if (currentLocation.getLocationName().equals(location)) {
//                existingLocation = true;
//            }
//        }
//
//        if (!existingLocation) {
//            oldBird.setLocation(svc.formatLocation(location));
//        }
//
//        oldBird.setBirdName(name);
//        oldBird.setBirdDesc(desc);
//        oldBird.setSpecies(species);
//        oldBird.setHandler(handler);
//        
//
//        boolean duplicateImage = false;
//        int duplicateImgID = 1;
//
//        if (oldBird.getImgID() == 0) {
//            oldBird.setImgID(1);
//        }
//
//        if (newContent != null && !newContent.isEmpty()) {
//
//            if (newContent.endsWith("jpg") || newContent.endsWith("png") || newContent.endsWith("jpeg")) {
//
//                List<Image> allImages = idao.getAllImages();
//
//                for (Image currentImage : allImages) {
//                    if (currentImage.getContent().equals(newContent)) {
//                        duplicateImage = true;
//                        duplicateImgID = currentImage.getImgID();
//                        oldBird.setImgID(duplicateImgID);
//                    }
//                }
//
//                if (!duplicateImage) {
//                    oldBird.setImgID(svc.formatImage(newContent));
//                }
//
//            }
//        }
//
//        bdao.updateBird(oldBird);
//
//        return "redirect:falcons";
//    }
//
//    @RequestMapping(value = "/displayFalcon", method = RequestMethod.GET)
//    public String displayFalcon(HttpServletRequest request, Model model) {
//        try {
//            int birdID = Integer.parseInt(request.getParameter("birdID"));
//
//            Bird bird = bdao.getBirdByID(birdID);
//
//            String imageURL = "https://i.imgur.com/PyD1aEk.png";
//
//            if (bird.getImgID() != 0) {
//                imageURL = idao.getImage(bird.getImgID()).getContent();
//            }
//
//            model.addAttribute("bird", bird);
//            model.addAttribute("imageURL", imageURL);
//
//            return "falconDetails";
//
//        } catch (NullPointerException | IndexOutOfBoundsException e) {
//
//            model.addAttribute("exception", e);
//
//            return "uhoh";
//        }
//    }
//
//    @RequestMapping(value = "/deleteFalcon", method = RequestMethod.GET)
//    public String deleteFalcon(HttpServletRequest request, Model model) {
//
//        try {
//
//            int birdID = Integer.parseInt(request.getParameter("birdID"));
//            bdao.getBirdByID(birdID);
//            Bird bird = bdao.getBirdByID(birdID);
//            bdao.removeBird(bird);
//            return "redirect:falcons";
//
//        } catch (NullPointerException | IndexOutOfBoundsException e) {
//            model.addAttribute("exception", e);
//            return "uhoh";
//        }
//    }
//
//    @RequestMapping(value = "displayPost", method = RequestMethod.GET)
//    public String displayPost(HttpServletRequest request, Model model) {
//
//        int postID = Integer.parseInt(request.getParameter("postID"));
//
//        Post post = pdao.getPostById(postID);
//
//        List<Image> postImageList = idao.getPostImagesByPostID(postID);
//
//        model.addAttribute("postImageList", postImageList);
//        model.addAttribute("post", post);
//
//        return "blogPost";
//    }
//
//    @RequestMapping(value = "editBlogPost", method = RequestMethod.GET)
//    public String displayEditBlogPost(HttpServletRequest request, Model model) {
//
//        int postID = Integer.parseInt(request.getParameter("postID"));
//
//        Post post = pdao.getPostById(postID);
//
//        List<Image> postImageList = new ArrayList<>();
//
//        List<Tag> postTagList = new ArrayList<>();
//
//        postTagList = tdao.getPostTagsByPostID(postID);
//
//        String postTagString = svc.deformatTags(postTagList);
//
//        postImageList = idao.getPostImagesByPostID(postID);
//
//        model.addAttribute("postImageList", postImageList);
//        model.addAttribute("postTagString", postTagString);
//        model.addAttribute("post", post);
//        model.addAttribute("postID", postID);
//
//        return "editBlogPost";
//    }
//
//    @RequestMapping(value = "/all", method = RequestMethod.GET)
//    public String displayAllPosts(Model model) {
//
//        String resultTypeHeader = "All Posts";
//        List<Post> postList = pdao.getAllApprovedPosts();
//        reverse(postList);
//
//        model.addAttribute("resultTypeHeader", resultTypeHeader);
//        model.addAttribute("postList", postList);
//
//        return "posts";
//    }
//
//    @RequestMapping(value = "/falconry", method = RequestMethod.GET)
//    public String displayFalconryPosts(Model model) {
//
//        String resultTypeHeader = "Falconry Posts";
//        List<Post> postList = new ArrayList<>();
//
//        for (Post currentPost : pdao.getPostByCategory("Falconry")) {
//            if (currentPost.isApproved() == true) {
//                postList.add(currentPost);
//            }
//        }
//
//        if (postList.size() > 0) {
//            reverse(postList);
//            model.addAttribute("postList", postList);
//        } else {
//            resultTypeHeader = "Sorry, no posts in this category yet!";
//        }
//
//        model.addAttribute("resultTypeHeader", resultTypeHeader);
//
//        return "posts";
//    }
//
//    @RequestMapping(value = "/sightings", method = RequestMethod.GET)
//    public String displaySightingPosts(Model model) {
//
//        String resultTypeHeader = "Sighting Posts";
//        List<Post> postList = new ArrayList<>();
//
//        for (Post currentPost : pdao.getPostByCategory("Sightings")) {
//            if (currentPost.isApproved() == true) {
//                postList.add(currentPost);
//            }
//        }
//
//        if (postList.size() > 0) {
//            reverse(postList);
//            model.addAttribute("postList", postList);
//        } else {
//            resultTypeHeader = "Sorry, no posts in this category yet!";
//        }
//
//        model.addAttribute("resultTypeHeader", resultTypeHeader);
//
//        return "posts";
//    }
//
//    @RequestMapping(value = "/community", method = RequestMethod.GET)
//    public String displayCommunityPosts(Model model) {
//
//        String resultTypeHeader = "Community Posts";
//        List<Post> postList = new ArrayList<>();
//
//        for (Post currentPost : pdao.getPostByCategory("Community")) {
//            if (currentPost.isApproved() == true) {
//                postList.add(currentPost);
//            }
//        }
//
//        if (postList.size() > 0) {
//            reverse(postList);
//            model.addAttribute("postList", postList);
//        } else {
//            resultTypeHeader = "Sorry, no posts in this category yet!";
//        }
//
//        model.addAttribute("resultTypeHeader", resultTypeHeader);
//
//        return "posts";
//    }
//
//    @RequestMapping(value = "/searchResults", method = RequestMethod.GET)
//    public String displaySearchResults(HttpServletRequest request, Model model) {
//
//        String searchTerm = request.getParameter("search");
//
//        String resultTypeHeader = "Search results for: '" + searchTerm + "'";
//        List<Post> byTags = pdao.getPostByTag('#' + searchTerm);
//        List<Post> byAuthors = pdao.getPostByUser(searchTerm);
//        List<Post> byTitles = pdao.getPostByTitle(searchTerm);
//
//        List<Post> postList = new ArrayList<>();
//        Set<Post> filterDuplicates = new HashSet<>();
//
//        for (Post currentPost : byTags) {
//            if (currentPost.isApproved() == true) {
//                postList.add(currentPost);
//            }
//        }
//        for (Post currentPost : byAuthors) {
//            if (currentPost.isApproved() == true) {
//                postList.add(currentPost);
//            }
//        }
//        for (Post currentPost : byTitles) {
//            if (currentPost.isApproved() == true) {
//                postList.add(currentPost);
//            }
//        }
//
//        filterDuplicates.addAll(postList);
//        postList.clear();
//        postList.addAll(filterDuplicates);
//
//        if (postList.size() > 0) {
//            reverse(postList);
//            model.addAttribute("postList", postList);
//        } else {
//            resultTypeHeader = "No results found for: '" + searchTerm + "'";
//        }
//
//        model.addAttribute("resultTypeHeader", resultTypeHeader);
//
//        return "posts";
//
//    }
//
//    @RequestMapping(value = "/new", method = RequestMethod.GET)
//    public String displayNewPostPage(Model model) {
//
//        List<Category> categoryList = cdao.getAllCategories();
//
//        model.addAttribute("categoryList", categoryList);
//
//        return "newPost";
//    }
//
//    @RequestMapping(value = "/createPost", method = RequestMethod.POST)
//    public String createPost(HttpServletRequest request, Model model) {
//        try {
//
//            Post post = new Post();
//
//            post.setTitle(request.getParameter("postTitle"));
//            String tagNamesBulk = request.getParameter("tags");
//
//            String[] tagNames = tagNamesBulk.split(",");
//            String[] categoryNames = request.getParameterValues("categories");
//
//            LocalDate postDate = LocalDate.now();
//            String dString = postDate.toString();
//            post.setDate(dString);
//            String def = "default";
//
//            ArrayList<String> fixedTagNames = svc.formatTags(tagNames);
//
//            List<Tag> tagList = new ArrayList<>();
//
//            for (String currentTag : fixedTagNames) {
//                Tag tagToAdd = new Tag();
//                tagToAdd.setName(currentTag);
//                tagList.add(tdao.addTag(tagToAdd));
//            }
//
//            post.setTags(tagList);
//
//            List<User> users = new ArrayList<>();
//            String userName = request.getParameter("userID");
//            users.add(udao.getUserByName(userName));
//
//            post.setUsers(users);
//
//            List<Category> categoryList = new ArrayList<>();
//
//            for (String currentCatName : categoryNames) {
//                Category catToAdd = cdao.getCategoryByName(currentCatName);
//                categoryList.add(catToAdd);
//            }
//            post.setCategories(categoryList);
//
//            //IMAGE PARSING STARTS HERE
//            String postBody = request.getParameter("newPostBody");
//
//            //THE FIRST SVC METHOD PARSES OUT THE IMAGES AND ADDS TO DB
//            List<Image> images = svc.parseImgTags(postBody);
//
//            //SECOND ONE REMOVES ALL IMAGE TAGS AND CONTENT WITHIN THEM
//            String processedPostBody = svc.removeImageTags(postBody);
//
//            post.setPostBody(processedPostBody);
//
//            post.setImages(images);
//
//            pdao.addPost(post);
//
//            return "redirect:home";
//
//        } catch (NullPointerException | IndexOutOfBoundsException e) {
//            model.addAttribute("exception", e);
//            return "uhoh";
//
//        }
//    }
//
//    @RequestMapping(value = "/updatePost", method = RequestMethod.POST)
//    public String updatePost(HttpServletRequest request, Model model) {
//        try {
//
//            int postID = parseInt(request.getParameter("postID"));
//
//            Post post = pdao.getPostById(postID);
//
//            post.setTitle(request.getParameter("postTitle"));
//
//            String tagNamesBulk = request.getParameter("tags");
//
//            String[] tagNames = tagNamesBulk.split(",");
//
//            LocalDate postDate = LocalDate.now();
//            String dString = postDate.toString();
//            post.setDate(dString);
//
//            ArrayList<String> fixedTagNames = svc.formatTags(tagNames);
//
//            List<Tag> tagList = new ArrayList<>();
//
//            for (String currentTag : fixedTagNames) {
//                Tag tagToAdd = new Tag();
//                tagToAdd.setName(currentTag);
//                tagList.add(tdao.addTag(tagToAdd));
//            }
//
//            post.setTags(tagList);
//
//            //IMAGE PARSING STARTS HERE
//            String postBody = request.getParameter("postBody");
//
////            List<Image> images = new ArrayList<>();
//            //THE FIRST SVC METHOD PARSES OUT THE IMAGES AND ADDS TO DB
//            List<Image> images = svc.parseImgTags(postBody);
//
//            //SECOND ONE REMOVES ALL IMAGE TAGS AND CONTENT WITHIN THEM
//            String processedPostBody = svc.removeImageTags(postBody);
//
//            post.setPostBody(processedPostBody);
//
////            post.setPostBody(postBody);
////            List<Image> allImages = post.getImages();
////            
////            for (Image image : images) {
////            allImages.add(image);
////                    }
////            
//            post.setImages(images);
//
//            post.setUsers(post.getUsers());
//
//            post.setCategories(post.getCategories());
//
//            post.setComments(post.getComments());
//
//            pdao.updatePost(post);
//
//            return "redirect:home";
//
//        } catch (NullPointerException | IndexOutOfBoundsException e) {
//            model.addAttribute("exception", e);
//            return "uhoh";
//
//        }
//    }
//
//    @RequestMapping(value = "removeComment", method = RequestMethod.GET)
//    public String removeComment(HttpServletRequest request, Model model) {
//
//        int commentID = Integer.parseInt(request.getParameter("commentID"));
//        try {
//            Comment comment = comDao.getComment(commentID);
//            comDao.removeComment(comment);
//        } catch (NullPointerException e) {
//            Comment comment = nucdao.getComment(commentID);
//            comDao.removeComment(comment);
//        }
//        int postID = Integer.parseInt(request.getParameter("postID"));
//
//        Post post = pdao.getPostById(postID);
//
//        model.addAttribute("post", post);
//
//        return "blogPost";
//    }
//
//    @RequestMapping(value = "addComment", method = RequestMethod.GET)
//    public String addComment(HttpServletRequest request, Model model) {
//
//        Comment comment = new Comment();
//
//        comment.setContent(request.getParameter("commentsArea"));
//        int postID = Integer.parseInt(request.getParameter("postID"));
//        comment.setPostID(postID);
//        User user = new User();
//        try {
//            user = udao.getUserByName(request.getParameter("userID"));
//            comment.setUser(user);
//            comDao.addComment(comment);
//        } catch (EmptyResultDataAccessException e) {
//            user.setUserName(request.getParameter("userID"));
//            comment.setUser(user);
//            nucdao.addcomment(comment);
//        }
//
//        Post post = pdao.getPostById(postID);
//
//        model.addAttribute("post", post);
//
//        return "blogPost";
//    }
}
