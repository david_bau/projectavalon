/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.romancingsaga2.dao;

import com.sg.romancingsaga2.model.PC;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.inject.Inject;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class PCDaoJdbcIml implements PCDao {

    private JdbcTemplate jdbcTemplate;

    @Inject
    public PCDaoJdbcIml(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_ADD_CHARACTER
            = "INSERT INTO characters (charname, class, female, undead, ghost, flying, race, sparktype, lp, str, dex, mag, logic, spd, con, spellpower, sword, spear, axe, bow, unarmed, fire, water, wind, earth, light, dark)"
            + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public PC addPC(PC pc) throws InvalidFormatException {

        // First do the insert
        jdbcTemplate.update(SQL_ADD_CHARACTER,
                pc.getCharname(),
                pc.getCharclass(),
                pc.isFemale(),
                pc.isUndead(),
                pc.isGhost(),
                pc.isFlying(),
                pc.getRace(),
                pc.getSparktype(),
                pc.getLp(),
                pc.getStr(),
                pc.getDex(),
                pc.getMag(),
                pc.getLogic(),
                pc.getSpd(),
                pc.getCon(),
                pc.getSpellpower(),
                pc.getSword(),
                pc.getSpear(),
                pc.getAxe(),
                pc.getBow(),
                pc.getUnarmed(),
                pc.getFire(),
                pc.getWater(),
                pc.getWind(),
                pc.getEarth(),
                pc.getLight(),
                pc.getDark());

        // Then get the id
        int myId = jdbcTemplate.queryForObject("select last_insert_id()", Integer.class);
        // Then stuff it in the object
        pc.setPcid(myId);
        return pc;
    }

    private static final String SQL_GET_ALL_CHARACTERS = "SELECT * FROM characters";

    @Override
    public int getLastPCIDInserted() {

        int pcID;
        pcID = jdbcTemplate.queryForObject("select last_insert_id()", Integer.class);
        return pcID;

    }

    @Override
    public List<PC> getAllPCs() {
        List<PC> pcList = jdbcTemplate.query(SQL_GET_ALL_CHARACTERS,
                new PCMapper());

//        for (Hero currentHero : heroesList) {
//            List<Integer> powerids = jdbcTemplate.queryForList(SQL_SELECT_POWERS_OF_HERO,
//                    Integer.class,
//                    currentHero.getHeroid());
//            List<Power> powersList = new ArrayList<>();
//            for (int currentInt : powerids) {
//                powersList.add(jdbcTemplate.queryForObject(SQL_SELECT_POWERS,
//                        new PowerMapper(),
//                        currentInt));
//            }
//            currentHero.setPowers(powersList);
//        }
        return pcList;
    }

    private static final String SQL_GET_character_BY_characterID = "SELECT * FROM characters WHERE pcid = ?";

    @Override
    public PC getPCById(int pcid) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_character_BY_characterID, new PCMapper(), pcid);

        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private static final String SQL_GET_character_BY_charname = "SELECT * FROM characters WHERE charname = ?";

    @Override
    public PC getPCByName(String charname) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_character_BY_charname, new PCMapper(), charname);

        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<PC> getPCsOfClass(String charClass) {

        List<PC> classedList = jdbcTemplate.query(SQL_GET_character_BY_charClass,
                new PCMapper(), charClass);

        return classedList;
    }

    @Override
    public List<PC> sortLoadedPCs(List<PC> PCs) {

        List<PC> classedList = jdbcTemplate.query(SQL_GET_character_BY_charClass,
                new PCMapper(), PCs);

        return classedList;
    }

    public static final String SQL_GET_character_BY_charClass = "SELECT * FROM characters WHERE charClass = ?";

    private static final String SQL_UPDATE_CHARACTER
            = "update characters set charname = ?, class = ?, female = ?, undead = ?, ghost = ?, flying = ?, race = ?, sparktype = ?, lp = ?, str = ?, dex = ?, mag = ?, logic = ?, spd = ?, con = ?, spellpower = ?, sword = ?, spear = ?, axe = ?, bow = ?, unarmed = ?, fire = ?, water = ?, wind = ?, earth = ?, light = ?, dark = ? where pcID =  ?";

    @Override
    public void updatePC(PC pc) {

        jdbcTemplate.update(SQL_UPDATE_CHARACTER,
                pc.getCharname(),
                pc.getCharclass(),
                pc.isFemale(),
                pc.isUndead(),
                pc.isGhost(),
                pc.isFlying(),
                pc.getRace(),
                pc.getSparktype(),
                pc.getLp(),
                pc.getStr(),
                pc.getDex(),
                pc.getMag(),
                pc.getLogic(),
                pc.getSpd(),
                pc.getCon(),
                pc.getSpellpower(),
                pc.getSword(),
                pc.getSpear(),
                pc.getAxe(),
                pc.getBow(),
                pc.getUnarmed(),
                pc.getFire(),
                pc.getWater(),
                pc.getWind(),
                pc.getEarth(),
                pc.getLight(),
                pc.getDark(),
                pc.getPcid());

    }

    private static final String SQL_DELETE_character
            = "delete from characters where charid = ?";

    @Override
    public void removePC(PC pc) {
        jdbcTemplate.update(SQL_DELETE_character, pc.getPcid());
    }

    public final class PCMapper implements RowMapper<PC> {

        //need to include locatio mapper based off locatio ID from SQL table
        @Override
        public PC mapRow(ResultSet rs, int i) throws SQLException {
            PC pcFromRecord = new PC();

            pcFromRecord.setPcid(rs.getInt("pcid"));
            pcFromRecord.setCharname(rs.getString("charname"));
            pcFromRecord.setCharclass(rs.getString("charclass"));
            pcFromRecord.setFemale(rs.getBoolean("female"));

            pcFromRecord.setUndead(rs.getBoolean("undead"));
            pcFromRecord.setGhost(rs.getBoolean("ghost"));
            pcFromRecord.setFlying(rs.getBoolean("flying"));
            pcFromRecord.setRace(rs.getString("race"));
            pcFromRecord.setSparktype(rs.getInt("sparktype"));

            pcFromRecord.setLp(rs.getInt("lp"));
            pcFromRecord.setStr(rs.getInt("str"));
            pcFromRecord.setDex(rs.getInt("dex"));
            pcFromRecord.setMag(rs.getInt("mag"));
            pcFromRecord.setLogic(rs.getInt("logic"));

            pcFromRecord.setSpd(rs.getInt("spd"));
            pcFromRecord.setCon(rs.getInt("con"));
            pcFromRecord.setSpellpower(rs.getInt("spellpower"));
            pcFromRecord.setSword(rs.getInt("sword"));
            pcFromRecord.setSpear(rs.getInt("spear"));

            pcFromRecord.setAxe(rs.getInt("axe"));
            pcFromRecord.setBow(rs.getInt("bow"));
            pcFromRecord.setUnarmed(rs.getInt("unarmed"));
            pcFromRecord.setFire(rs.getInt("fire"));
            pcFromRecord.setWater(rs.getInt("water"));
            pcFromRecord.setWind(rs.getInt("wind"));
            pcFromRecord.setEarth(rs.getInt("earth"));
            pcFromRecord.setLight(rs.getInt("light"));
            pcFromRecord.setDark(rs.getInt("dark"));

//            Location loc = new Location();
//
//            loc.setLocationID(rs.getInt("locationID"));
//            loc.setLocationName(rs.getString("locationName"));
//            loc.setLocationDesc(rs.getString("locationDesc"));
//            loc.setLatitude(rs.getBigDecimal("latitude"));
//            loc.setLongitude(rs.getBigDecimal("longitude"));
//
//            pcFromRecord.setLocation(loc);
            return pcFromRecord;
        }
    }

}
