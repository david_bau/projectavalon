/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.romancingsaga2.dao;

/**
 *
 * @author dbb09
 */
public class InvalidFormatException extends Exception {

public InvalidFormatException(String message) {
    
        super(message);
        
    }
    public InvalidFormatException(String message, Throwable cause) {
        
        super(message, cause);
        
    }  
}
