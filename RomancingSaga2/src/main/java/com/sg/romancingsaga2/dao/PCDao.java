/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.romancingsaga2.dao;

import com.sg.romancingsaga2.model.PC;
import java.util.List;

/**
 *
 * @author dbb09
 */
public interface PCDao {

    //CREATE
    public PC addPC(PC pc) throws InvalidFormatException;

    // READ
    public List<PC> getAllPCs();

    public PC getPCById(int pcid);

    public List<PC> getPCsOfClass(String charClass);

    public List<PC> sortLoadedPCs(List<PC> PCs);

    public PC getPCByName(String charname);

    public int getLastPCIDInserted();

//    public PC getPCByID(int pcid);
    // UPDATE
    public void updatePC(PC pc);

    // DELETE
    public void removePC(PC pc);

}
