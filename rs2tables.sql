DROP DATABASE IF EXISTS rs2;

CREATE DATABASE rs2;

use rs2;

CREATE TABLE characters(
  pcid int(3) UNIQUE NOT NULL AUTO_INCREMENT,
  charname varchar(50) UNIQUE NOT NULL,
  charclass varchar(50),
  female binary(1) DEFAULT NULL,
  undead binary(1) DEFAULT NULL,
  ghost binary(1) DEFAULT NULL,
  flying binary(1) DEFAULT NULL,
  race varchar(50),
  sparktype smallint(20) DEFAULT NULL,
  lp tinyint(11) DEFAULT NULL,
  str tinyint(11) DEFAULT NULL,
  dex tinyint(11) DEFAULT NULL,
  mag tinyint(11) DEFAULT NULL,
  logic tinyint(11) DEFAULT NULL,
  spd tinyint(11) DEFAULT NULL,
  con tinyint(11) DEFAULT NULL,
  spellpower tinyint(11) DEFAULT NULL,
  sword tinyint(11) DEFAULT NULL,
  spear tinyint(11) DEFAULT NULL,
  axe tinyint(11) DEFAULT NULL,
  bow tinyint(11) DEFAULT NULL,
  unarmed tinyint(11) DEFAULT NULL,
  fire tinyint(11) DEFAULT NULL,
  water tinyint(11) DEFAULT NULL,
  wind tinyint(11) DEFAULT NULL,
  earth tinyint(11) DEFAULT NULL,
  light tinyint(11) DEFAULT NULL,
  dark tinyint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE techs (
    `Techid` INT,
    `Name` VARCHAR(26) CHARACTER SET utf8,
    `Weapon_Type` VARCHAR(11) CHARACTER SET utf8,
    `Power` VARCHAR(21) CHARACTER SET utf8,
    `Special_Notes` VARCHAR(136) CHARACTER SET utf8,
    `Type` VARCHAR(17) CHARACTER SET utf8,
    `Accuracy` VARCHAR(6) CHARACTER SET utf8,
    `Flat_Evasion` INT,
    `Use_Shield` VARCHAR(1) CHARACTER SET utf8,
    `Counter` VARCHAR(1) CHARACTER SET utf8,
    `Evade` INT,
    `Evade_Difficulty` VARCHAR(3) CHARACTER SET utf8
);


CREATE TABLE monsters (
    `Monsterid` INT,
    `MonsterName` VARCHAR(20) CHARACTER SET utf8,
    `Battle_Number` INT,
    `Enemy_Type` VARCHAR(8) CHARACTER SET utf8,
    `Level` INT,
    `Name` VARCHAR(20) CHARACTER SET utf8,
    `Experience` INT,
    `HP` INT,
    `LP` INT,
    `WP` INT,
    `JP` INT,
    `Strength` INT,
    `Dexterity` INT,
    `Magic` INT,
    `Darkness` INT,
    `Speed` INT,
    `Constitution` INT,
    `Tech_Level` INT,
    `Magic_Level` INT,
    `Slash` INT,
    `Bludgeon` INT,
    `Pierce` INT,
    `Arrow` INT,
    `Heat` INT,
    `Cold` INT,
    `Electric` INT,
    `Status` INT,
    `Shield_D` INT,
    `Evade` INT,
    `Slashb` INT,
    `Bludgeonb` INT,
    `Pierceb` INT,
    `Arrowb` INT,
    `Heatb` INT,
    `Coldb` INT,
    `Electricb` INT,
    `Statusb` INT,
    `Death` INT,
    `Paralysis` INT,
    `Sleep` INT,
    `Blind` INT,
    `Mental` INT,
    `Poison` INT,
    `Pressure_Wave` INT,
    `Stun` INT,
    `Female` INT,
    `Undead` INT,
    `Boss` INT,
    `Flying` INT,
    `Race` VARCHAR(15) CHARACTER SET utf8,
    `High_Item` VARCHAR(19) CHARACTER SET utf8,
    `Low_Item` VARCHAR(20) CHARACTER SET utf8,
    `Weapon_1` VARCHAR(17) CHARACTER SET utf8,
    `Weapon_2` VARCHAR(17) CHARACTER SET utf8
);

CREATE TABLE techstypes (
    `Techid` INT,
    `Typeid` INT
);
